create table producto(
	id bigint not null primary key,
	nombre varchar(50) not null,
	marca varchar(20) not null,
	precio double not null
)