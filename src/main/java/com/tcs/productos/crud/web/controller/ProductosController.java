package com.tcs.productos.crud.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.productos.crud.web.bean.ProductoBean;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import com.tcs.productos.crud.core.service.ProductoService;

@RestController
public class ProductosController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductosController.class);
	
	@Autowired
	private ProductoService service;
	
	@ApiOperation(value = "Agrega un nuevo producto a la tabla")
	@ApiResponses(value = {@ApiResponse(code = 201, message = "Producto creado correctamente"),
						   @ApiResponse(code = 500, message = "Ocurrio un error inesperado")})
	@RequestMapping(value = "/producto",
					produces = MediaType.APPLICATION_JSON_VALUE,
					method = RequestMethod.POST)
	public ResponseEntity<ProductoBean> crearProducto(@RequestBody ProductoBean bean){
		try {
			return new ResponseEntity<ProductoBean>(service.crear(bean), HttpStatus.CREATED);
		} catch(Exception e) {
			LOGGER.error("Error creando producto", e);
			return new ResponseEntity<ProductoBean>(bean, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation(value = "Lista todos los productos existentes en la tabla")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "Producto creado correctamente"),
						   @ApiResponse(code = 500, message = "Ocurrio un error inesperado")})
	@RequestMapping(value = "/producto",
					produces = MediaType.APPLICATION_JSON_VALUE,
					method = RequestMethod.GET)
	public ResponseEntity<List<ProductoBean>> listarProductos(){
		try {
			return new ResponseEntity<List<ProductoBean>>(service.listar(), HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error("Error al listar los productos", e);
			return new ResponseEntity<List<ProductoBean>>(new ArrayList<ProductoBean>(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation(value = "Obtiene el detalle de un producto dado un ID")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "Producto creado correctamente"),
						   @ApiResponse(code = 500, message = "Ocurrio un error inesperado")})
	@RequestMapping(value = "/producto/{id}",
					produces = MediaType.APPLICATION_JSON_VALUE,
					method = RequestMethod.GET)
	public ResponseEntity<ProductoBean> obtenerProducto(@PathVariable("id") Long id){
		try {
			return new ResponseEntity<ProductoBean>(service.obtener(id), HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error("Error al obtener producto", e);
			return new ResponseEntity<ProductoBean>(new ProductoBean(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation(value = "Actualiza los datos de un producto dado su ID")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "Producto creado correctamente"),
						   @ApiResponse(code = 500, message = "Ocurrio un error inesperado")})
	@RequestMapping(value = "/producto",
					produces = MediaType.APPLICATION_JSON_VALUE,
					method = RequestMethod.PUT)
	public ResponseEntity<ProductoBean> actualizarProducto(@RequestBody ProductoBean bean){
		try {
			return new ResponseEntity<ProductoBean>(service.actualizar(bean), HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error("Error al actualizar producto", e);
			return new ResponseEntity<ProductoBean>(new ProductoBean(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}