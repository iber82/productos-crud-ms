package com.tcs.productos.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductosCrudMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductosCrudMsApplication.class, args);
	}

}
