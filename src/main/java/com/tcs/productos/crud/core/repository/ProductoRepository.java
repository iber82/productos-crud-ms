package com.tcs.productos.crud.core.repository;

import org.springframework.data.repository.CrudRepository;

import com.tcs.productos.crud.core.domain.Producto;

public interface ProductoRepository extends CrudRepository<Producto, Long>{

}