package com.tcs.productos.crud.core.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tcs.productos.crud.web.bean.ProductoBean;
import com.tcs.productos.crud.core.domain.Producto;
import com.tcs.productos.crud.core.repository.ProductoRepository;

@Service
public class ProductoService {

	@Autowired
	private ProductoRepository repository;
	
	public ProductoBean crear(ProductoBean bean) {
		Producto producto = obtenerProducto(bean);
		producto = repository.save(producto);
		bean.setId(producto.getId());
		return bean;
	}
	
	public List<ProductoBean> listar(){
		List<ProductoBean> productosBean = new ArrayList<ProductoBean>();
		repository.findAll().forEach(producto -> productosBean.add(obtenerBean(producto)));
		return productosBean;
	}
	
	public ProductoBean obtener(Long id) {
		Optional<Producto> producto = repository.findById(id);
		if(producto.isPresent()){
			return obtenerBean(repository.findById(id).get());
		}
		return null;
	}
	
	public ProductoBean actualizar(ProductoBean bean) {
		Producto producto = obtenerProducto(bean);
		Optional<Producto> busqueda = repository.findById(producto.getId());
		if(busqueda.isPresent()) {
			return obtenerBean(repository.save(producto));
		}
		return null;
	}
	
	/*
	 * Obtiene un objeto producto web a partir de un objeto producto de dominio de datos
	 */
	private ProductoBean obtenerBean(Producto producto) {
		ProductoBean bean = new ProductoBean();
		bean.setCantidad(producto.getCantidad());
		bean.setId(producto.getId());
		bean.setMarca(producto.getMarca());
		bean.setNombre(producto.getNombre());
		bean.setPrecio(producto.getPrecio());
		return bean;
	}

	/*
	 * Obtiene un objeto producto de dominio de datos a partir de un objeto producto web
	 */
	private Producto obtenerProducto(ProductoBean bean) {
		Producto producto = new Producto();
		producto.setId(bean.getId());
		producto.setCantidad(bean.getCantidad());
		producto.setMarca(bean.getMarca());
		producto.setNombre(bean.getNombre());
		producto.setPrecio(bean.getPrecio());
		return producto;
	}

}