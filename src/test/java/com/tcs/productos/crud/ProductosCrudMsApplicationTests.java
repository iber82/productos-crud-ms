package com.tcs.productos.crud;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tcs.productos.crud.core.service.ProductoService;
import com.tcs.productos.crud.web.bean.ProductoBean;

@SpringBootTest
@AutoConfigureMockMvc
class ProductosCrudMsApplicationTests {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ProductoService service;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Test
	void crearProducto() throws Exception {
		ProductoBean requestBean = new ProductoBean();
		requestBean.setNombre("Galaxy S20 Plus");
		requestBean.setMarca("Samsung");
		requestBean.setPrecio(new Double(850000));
		requestBean.setCantidad(5);
		
		MvcResult result = mockMvc.perform(post("/producto")
			 	  						   .contentType(MediaType.APPLICATION_JSON_VALUE)
			 	  						   .content(objectMapper.writeValueAsString(requestBean)))
			 	  				  .andExpect(status().isCreated())
			 	  				  .andDo(print())
			 	  				  .andReturn();
		ProductoBean responseBean = objectMapper.readValue(result.getResponse().getContentAsString(), ProductoBean.class);
		
		ProductoBean producto = service.obtener(responseBean.getId()); //Siempre sera ID 1 al usar h2 en memoria
		assertThat(producto.getNombre().equals(requestBean.getNombre()) && 
				   producto.getMarca().equals(requestBean.getMarca()));
	}

	@Test
	void listarProductos() throws Exception {
		crearProducto();
		MvcResult result = mockMvc.perform(get("/producto").contentType(MediaType.APPLICATION_JSON_VALUE))
			 	  				  .andExpect(status().isOk())
			 	  				  .andDo(print())
			 	  				  .andReturn();
		ProductoBean[] responseBean = objectMapper.readValue(result.getResponse().getContentAsString(), ProductoBean[].class);
		assertThat(responseBean != null && responseBean.length > 0);
	}

	@Test
	void obtenerProducto() throws Exception {
		crearProducto();
		ProductoBean responseBean = new ProductoBean();
		MvcResult result = mockMvc.perform(get("/producto/1").contentType(MediaType.APPLICATION_JSON_VALUE))
			 	  				  .andExpect(status().isOk())
			 	  				  .andDo(print())
			 	  				  .andReturn();
		responseBean = objectMapper.readValue(result.getResponse().getContentAsString(), ProductoBean.class);
		assertThat(responseBean != null && !responseBean.getNombre().isEmpty());
	}

	@Test
	void actualizarProducto() throws Exception {
		crearProducto();
		ProductoBean requestBean = service.obtener(1L);
		requestBean.setMarca("Apple");
		requestBean.setNombre("iPhone 11 Pro");
		
		mockMvc.perform(put("/producto/")
							.contentType(MediaType.APPLICATION_JSON_VALUE)
				   			.content(objectMapper.writeValueAsString(requestBean)))
						.andExpect(status().isOk())
						.andDo(print());

		ProductoBean producto = service.obtener(1L);
		assertThat(producto.getMarca().equals(requestBean.getMarca()) &&
				   producto.getNombre().equals(requestBean.getNombre()));
	}

}